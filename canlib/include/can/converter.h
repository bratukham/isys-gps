#ifndef __ISYS_CAN_CONVERTER_H__
#define __ISYS_CAN_CONVERTER_H__

#include "can/convert.h"

#ifdef  __cplusplus
extern "C" {
#endif

typedef void (*canConverterCallback) (const canINFO * can);

int get_can_from_gps(const char * message, canConverterCallback callback);

#ifdef  __cplusplus
}
#endif


#endif /* __ISYS_CAN_CONVERTER_H__ */
