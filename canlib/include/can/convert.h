#ifndef __ISYS_CAN_CONVERT_H__
#define __ISYS_CAN_CONVERT_H__


#include "nmea/sentence.h"
#include <stdbool.h>
#include "driverlib/can.h"


#ifdef  __cplusplus
extern "C" {
#endif


#define STANDART_GPS_MESSAGES_TYPE_ID				(0x00001000)


// GPS GPGGA Message Type IDs
#define GPS_GPGGA_TYPE_ID							(STANDART_GPS_MESSAGES_TYPE_ID | 0x00000100)
#define GPS_GPGGA_TIME_PART_ID 						(GPS_GPGGA_TYPE_ID | 0x00000001)
#define GPS_GPGGA_QUALITY_AND_SATELLITES_ID 		(GPS_GPGGA_TYPE_ID | 0x00000002)
#define GPS_GPGGA_LATITUDE_ID 						(GPS_GPGGA_TYPE_ID | 0x00000003)
#define GPS_GPGGA_LONGITUDE_ID 						(GPS_GPGGA_TYPE_ID | 0x00000004)
#define GPS_GPGGA_ALTITUDE_ID 						(GPS_GPGGA_TYPE_ID | 0x00000005)
#define GPS_GPGGA_HDOP_ID 							(GPS_GPGGA_TYPE_ID | 0x00000006)


// GPS GPVTG Message Type IDs
#define GPS_GPVTG_TYPE_ID							(STANDART_GPS_MESSAGES_TYPE_ID | 0x00000200)
#define GPS_GPVTG_DIR_ID 							(GPS_GPVTG_TYPE_ID | 0x00000001)
#define GPS_GPVTG_DEC_ID 							(GPS_GPVTG_TYPE_ID | 0x00000002)
#define GPS_GPVTG_SPN_ID 							(GPS_GPVTG_TYPE_ID | 0x00000003)
#define GPS_GPVTG_SPK_ID 							(GPS_GPVTG_TYPE_ID | 0x00000004)


#define DEFAULT_CAN_MESSAGE_FLAGS					(MSG_OBJ_TX_INT_ENABLE | MSG_OBJ_EXTENDED_ID)


typedef struct _canINFO
{
	int msg_count;
	tCANMsgObject *messages;
} canINFO;


int		can_zero_info(canINFO * info);
int		can_destroy_info(canINFO * info);
int 	can_create_gpgga_info(nmeaGPGGA * gpgga, canINFO * info);
int 	can_create_gpvtg_info(nmeaGPVTG * gpvtg, canINFO * info);

int 	_can_create_message(uint32_t msg_id, uint32_t msg_id_mask, uint32_t flags, uint32_t length, uint8_t * data, tCANMsgObject * msg);


#ifdef  __cplusplus
}
#endif


#endif /* __ISYS_CAN_CONVERT_H__ */
