#include <stdint.h>
#include <stdlib.h>
#include "nmea/sentence.h"
#include "nmea/parser.h"
#include "can/converter.h"
#include "can/convert.h"


/**
 * \brief Parse a valid message of GPS (like GPGGA, GPVTG) and convert it info array of the CAN messages
 * \and after converting it calls callback function with CAN messages
 * @return true (1) - success or false (0) - fail
 */
int
get_can_from_gps(const char * message, canConverterCallback callback)
{
	canINFO can;

	int length = strlen(message);

	if (strstr(message, "$GPGGA") != NULL)
	{
		nmeaGPGGA gpgga;

		can_create_gpgga_info(&gpgga, &can);

		callback(&can);

		can_destroy_info(&can);
	}
	else if (strstr(message, "$GPVTG") != NULL)
	{
		nmeaGPVTG gpvtg;
		nmea_parse_GPVTG(message, length, &gpvtg);
		can_create_gpvtg_info(&gpvtg, &can);

		callback(&can);

		can_destroy_info(&can);
	}
	else
	{
		// TODO #HEADINGA
	}

	return 1;
}
