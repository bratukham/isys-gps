#include <stdlib.h>
#include <stdint.h>
#include "nmea/sentence.h"
#include "can/convert.h"


/**
 * \brief Initialization of CAN info
 * @return true (1) - success or false (0) - fail
 */
int
can_zero_info(canINFO * info)
{
	if (info == NULL)
	{
		return 0;
	}

	info->msg_count = 0;
	info->messages = NULL;

	return 1;
}


/**
 * \brief Destroy of CAN info
 * @return true (1) - success or false (0) - fail
 */
int
can_destroy_info(canINFO * info)
{
	int it;

	if (info->messages == NULL)
	{
		return 0;
	}

	for (it = 0; it < info->msg_count; ++it)
	{
		if (info->messages[it].pui8MsgData != NULL)
		{
			free(info->messages[it].pui8MsgData);
		}
	}

	free(info->messages);

	info->messages = NULL;

	return 1;
}


/**
 * \brief Creation of the CAN messages from the GPGGA message
 * @return true (1) - success or false (0) - fail
 */
int
can_create_gpgga_info(nmeaGPGGA * gpgga, canINFO * info)
{
	int it;
	char buffer[256];

	if (info == NULL)
	{
		return 0;
	}

	info->msg_count = 6;
	info->messages = malloc(sizeof (tCANMsgObject) * info->msg_count);

	sprintf(buffer, "%2d%2d%2d.%2d", gpgga->utc.hour, gpgga->utc.min, gpgga->utc.sec, gpgga->utc.hsec);

	double time = atof(buffer);
	uint32_t quality_and_satellites = (gpgga->sig << 16) | gpgga->satinuse;

	_can_create_message(GPS_GPGGA_TIME_PART_ID, 0, DEFAULT_CAN_MESSAGE_FLAGS, 8, &time, &info->messages[0]);
	_can_create_message(GPS_GPGGA_QUALITY_AND_SATELLITES_ID, 0, DEFAULT_CAN_MESSAGE_FLAGS, 4, &quality_and_satellites, &info->messages[1]);
	_can_create_message(GPS_GPGGA_LATITUDE_ID, 0, DEFAULT_CAN_MESSAGE_FLAGS, 8, &gpgga->lat, &info->messages[2]);
	_can_create_message(GPS_GPGGA_LONGITUDE_ID, 0, DEFAULT_CAN_MESSAGE_FLAGS, 8, &gpgga->lon, &info->messages[3]);
	_can_create_message(GPS_GPGGA_ALTITUDE_ID, 0, DEFAULT_CAN_MESSAGE_FLAGS, 8, &gpgga->elv, &info->messages[4]);
	_can_create_message(GPS_GPGGA_HDOP_ID, 0, DEFAULT_CAN_MESSAGE_FLAGS, 8, &gpgga->HDOP, &info->messages[5]);

	return 1;
}


/**
 * \brief Creation of the CAN messages from the GPVTG message
 * @return true (1) - success or false (0) - fail
 */
int
can_create_gpvtg_info(nmeaGPVTG * gpvtg, canINFO * info)
{
	int it;

	if (info == NULL)
	{
		return 0;
	}

	info->msg_count = 4;
	info->messages = malloc(sizeof (tCANMsgObject) * info->msg_count);

	_can_create_message(GPS_GPVTG_DIR_ID, 0, DEFAULT_CAN_MESSAGE_FLAGS, 8, &gpvtg->dir, &info->messages[0]);
	_can_create_message(GPS_GPVTG_DEC_ID, 0, DEFAULT_CAN_MESSAGE_FLAGS, 8, &gpvtg->dec, &info->messages[1]);
	_can_create_message(GPS_GPVTG_SPN_ID, 0, DEFAULT_CAN_MESSAGE_FLAGS, 8, &gpvtg->spn, &info->messages[2]);
	_can_create_message(GPS_GPVTG_SPK_ID, 0, DEFAULT_CAN_MESSAGE_FLAGS, 8, &gpvtg->spk, &info->messages[3]);

	return 1;
}


/**
 * \brief Creation of a CAN message with a specified data
 * @return true (1) - success or false (0) - fail
 */
int
_can_create_message(uint32_t msg_id, uint32_t msg_id_mask, uint32_t flags, uint32_t length, uint8_t * data, tCANMsgObject * msg)
{
	int it;

	msg->ui32MsgID = msg_id;
	msg->ui32Flags = flags;
	msg->ui32MsgIDMask = msg_id_mask;
	msg->ui32MsgLen = length;
	msg->pui8MsgData = malloc(length);

	if (msg->pui8MsgData == NULL)
	{
		return 0;
	}

	for (it = 0; it < length; ++it)
	{
		msg->pui8MsgData[it] = data[it];
	}

	return 1;
}
