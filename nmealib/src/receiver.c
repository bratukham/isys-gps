/*
 *
 * Addon for NMEA library
 * Author: Mikhail Bratukha (bratukha.m@gmail.com)
 * Licence: http://www.gnu.org/licenses/lgpl.html
 *
 * receiver.c
 */


#include <stdlib.h>
#include "nmea/receiver.h"


/**
 * \brief Initialization of receiver object
 * @return true (1) - success or false (0) - fail
 */
int
nmea_receiver_init(nmeaRECEIVER * receiver)
{
	if (receiver == NULL)
	{
		return 0;
	}

	receiver->buff_size = NMEA_MIN_RECEIVEBUFF;
	receiver->buff_use = 0;
	receiver->buff_state = NMEA_RECEIVEBUFF_INVALID_DATA;
	receiver->buffer = malloc(sizeof (unsigned char) * receiver->buff_size);

	return 1;
}


/**
 * \brief Destroy receiver object
 * @return true (1) - success or false (0) - fail
 */
int
nmea_receiver_destroy(nmeaRECEIVER * receiver)
{
	if (receiver == NULL)
	{
		return 0;
	}

	if (receiver->buffer == NULL)
	{
		return 0;
	}

	free(receiver->buffer);

	return 1;
}


/**
 * \brief Clear the buffer of receiver object
 * @return true (1) - success or false (0) - fail
 */
int
nmea_receiver_clear_buff(nmeaRECEIVER * receiver)
{
	if (receiver == NULL || receiver->buffer == NULL)
	{
		return 0;
	}

	receiver->buff_use = 0;

	memset(receiver->buffer, 0, receiver->buff_size);

	receiver->buff_state = NMEA_RECEIVEBUFF_INVALID_DATA;

	return 1;
}


/**
 * \brief Put a byte into the buffer and call callback function if the buffered GPS massage is valid
 * @return true (1) - success or false (0) - fail
 */
int
nmea_receiver_put(nmeaRECEIVER * receiver, unsigned char byte, nmeaReceiverCallback callback)
{
	if (receiver == NULL || receiver->buffer == NULL)
	{
		return 0;
	}

	if (byte == '$')
	{
		nmea_receiver_clear_buff(receiver);
		receiver->buff_state ^= NMEA_RECEIVEBUFF_HAS_START_SYMB;
	}

	if (receiver->buff_use >= receiver->buff_size)
	{
		return 0;
	}

	receiver->buffer[receiver->buff_use] = byte;

	receiver->buff_use++;

	if (byte == '\r')
	{
		receiver->buff_state ^= NMEA_RECEIVEBUFF_HAS_CR_SYMB;
	}

	if (byte == '\n')
	{
		receiver->buff_state ^= NMEA_RECEIVEBUFF_HAS_LF_SYMB;
	}

	if (receiver->buff_state == NMEA_RECEIVEBUFF_HAS_VALID_MSG)
	{
		receiver->test_field++;

		if (callback != NULL)
		{
			callback(&receiver->buffer[0]);
		}

		nmea_receiver_clear_buff(receiver);
	}

	return 1;
}
