/*
 *
 * Addon for NMEA library
 * Author: Mikhail Bratukha (bratukha.m@gmail.com)
 * Licence: http://www.gnu.org/licenses/lgpl.html
 *
 * receiver.h
 */


#ifndef __NMEA_RECEIVER_H__
#define __NMEA_RECEIVER_H__


#ifdef  __cplusplus
extern "C" {
#endif


#define NMEA_MIN_RECEIVEBUFF				(512)


#define NMEA_RECEIVEBUFF_INVALID_DATA		(0x0000)
#define NMEA_RECEIVEBUFF_HAS_START_SYMB		(0x0001)
#define NMEA_RECEIVEBUFF_HAS_CR_SYMB		(0x0010)
#define NMEA_RECEIVEBUFF_HAS_LF_SYMB		(0x0100)
#define NMEA_RECEIVEBUFF_HAS_VALID_MSG		(NMEA_RECEIVEBUFF_HAS_START_SYMB | NMEA_RECEIVEBUFF_HAS_CR_SYMB | NMEA_RECEIVEBUFF_HAS_LF_SYMB)


typedef struct _nmeaRECEIVER
{
	int test_field;
	unsigned char *buffer;
	int buff_state;
	int buff_size;
	int buff_use;
} nmeaRECEIVER;


typedef void (*nmeaReceiverCallback) (const char * message);


int nmea_receiver_init(nmeaRECEIVER * receiver);
int nmea_receiver_destroy(nmeaRECEIVER * receiver);

int nmea_receiver_clear_buff(nmeaRECEIVER * receiver);
int nmea_receiver_put(nmeaRECEIVER * receiver, unsigned char byte, nmeaReceiverCallback callback);


#ifdef  __cplusplus
}
#endif


#endif /* __NMEA_RECEIVER_H__ */
