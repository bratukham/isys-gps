#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_ints.h"
#include "inc/hw_can.h"


#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "driverlib/can.h"
#include "utils/uartstdio.h"
#include "driverlib/can.h"
#include "can/can.h"

#include "nmea/nmea.h"
#include <string.h>

#define BUFF_SIZE 	4096
#define UART_SPEED 	115200
#define CPU_SPEED 	16000000


//*****************************************************************************
//
// The error routine that is called if the driver library encounters an error.
//
//*****************************************************************************
#ifdef DEBUG
void
__error__(char *pcFilename, uint32_t ui32Line)
{
}
#endif


//*****************************************************************************
//
// A flag to indicate that some transmission error occurred.
//
//*****************************************************************************
volatile bool g_bErrFlag = 0;

volatile unsigned int g_uiIntCount = 0;

volatile unsigned int g_uiCharCount = 0;

volatile unsigned int g_newCharOffset = 0;

volatile unsigned int g_readedCharOffset = 0;

volatile unsigned int g_validMessagesCount = 0;

unsigned char buff[BUFF_SIZE];

nmeaRECEIVER receiver;


//*****************************************************************************
//
// This function provides a some seconds delay using a simple polling method.
//
//*****************************************************************************
void
SimpleDelay(void)
{
    //
    // Delay set manually
    //
    SysCtlDelay(SysCtlClockGet() / (900 * 8));
}


void
send_to_can(const canINFO * can)
{
	int it;

	for (it = 0; it < can->msg_count; ++it)
	{
		CANMessageSet(CAN0_BASE, 1, &can->messages[it], MSG_OBJ_TYPE_TX);

		//
		// Now wait some seconds before continuing
		//
		SimpleDelay();
	}
}


void
gps_receiver_callback(const char * message)
{
	get_can_from_gps(message, send_to_can);
}


//*****************************************************************************
//
// Send a string to the UART.
//
//*****************************************************************************
void
UARTSend(const uint8_t *pui8Buffer, uint32_t ui32Count)
{
    //
    // Loop while there are more characters to send.
    //
    while(ui32Count--)
    {
        //
        // Write the next character to the UART.
        //
        ROM_UARTCharPutNonBlocking(UART0_BASE, *pui8Buffer++);
    }
}


//*****************************************************************************
//
// This function is the interrupt handler for the CAN peripheral.  It checks
// for the cause of the interrupt, and maintains a count of all messages that
// have been transmitted.
//
//*****************************************************************************
void
CANIntHandler(void)
{
    uint32_t ui32Status;
    UARTSend((uint8_t *)"CAN interrupt.\n", 14);
    //
    // Read the CAN interrupt status to find the cause of the interrupt
    //
    ui32Status = CANIntStatus(CAN0_BASE, CAN_INT_STS_CAUSE);

    //
    // If the cause is a controller status interrupt, then get the status
    //
    if(ui32Status == CAN_INT_INTID_STATUS)
    {
        //
        // Read the controller status.  This will return a field of status
        // error bits that can indicate various errors.  Error processing
        // is not done in this example for simplicity.  Refer to the
        // API documentation for details about the error status bits.
        // The act of reading this status will clear the interrupt.  If the
        // CAN peripheral is not connected to a CAN bus with other CAN devices
        // present, then errors will occur and will be indicated in the
        // controller status.
        //
        ui32Status = CANStatusGet(CAN0_BASE, CAN_STS_CONTROL);

        //
        // Set a flag to indicate some errors may have occurred.
        //
        g_bErrFlag = 1;
    }

    //
    // Check if the cause is message object 1, which what we are using for
    // sending messages.
    //
    else if(ui32Status == 1)
    {
        //
        // Getting to this point means that the TX interrupt occurred on
        // message object 1, and the message TX is complete.  Clear the
        // message object interrupt.
        //
        CANIntClear(CAN0_BASE, 1);

        //
        // Since the message was sent, clear any error flags.
        //
        g_bErrFlag = 0;
    }

    //
    // Otherwise, something unexpected caused the interrupt.  This should
    // never happen.
    //
    else
    {
        //
        // Spurious interrupt handling can go here.
        //
    }
}


//*****************************************************************************
//
// The UART interrupt handler.
//
//*****************************************************************************
void
UARTIntHandler(void)
{
    uint32_t ui32Status;
    unsigned char inChar;
    //
    // Get the interrupt status.
    //
    ui32Status = ROM_UARTIntStatus(UART4_BASE, true);

    //
    // Loop while there are characters in the receive FIFO.
    //
    while (ROM_UARTCharsAvail(UART4_BASE))
    {
        //
        // Read the next character from the UART and write it back to the UART.
        //
    	inChar = ROM_UARTCharGetNonBlocking(UART4_BASE);

    	buff[g_newCharOffset++] = inChar;
    	g_uiCharCount++;
    }

    UARTRxErrorClear(UART4_BASE);
    ROM_UARTIntClear(UART4_BASE, ui32Status);
}


void ConfigureCAN(void) {
	 //
	    // For this example CAN0 is used with RX and TX pins on port B4 and B5.
	    // The actual port and pins used may be different on your part, consult
	    // the data sheet for more information.
	    // GPIO port B needs to be enabled so these pins can be used.
	    //
	    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

	    //
	    // Configure the GPIO pin muxing to select CAN0 functions for these pins.
	    // This step selects which alternate function is available for these pins.
	    // This is necessary if your part supports GPIO pin function muxing.
	    // Consult the data sheet to see which functions are allocated per pin.
	    //
	    GPIOPinConfigure(GPIO_PE4_CAN0RX);
	    GPIOPinConfigure(GPIO_PE5_CAN0TX);

	    //
	    // Enable the alternate function on the GPIO pins.  The above step selects
	    // which alternate function is available.  This step actually enables the
	    // alternate function instead of GPIO for these pins.
	    //
	    GPIOPinTypeCAN(GPIO_PORTE_BASE, GPIO_PIN_4 | GPIO_PIN_5);

	    //
	    // The GPIO port and pins have been set up for CAN.  The CAN peripheral
	    // must be enabled.
	    //
	    SysCtlPeripheralEnable(SYSCTL_PERIPH_CAN0);

	    //
	    // Initialize the CAN controller
	    //
	    CANInit(CAN0_BASE);

	    //
	    // Set up the bit rate for the CAN bus.  This function sets up the CAN
	    // bus timing for a nominal configuration.  You can achieve more control
	    // over the CAN bus timing by using the function CANBitTimingSet() instead
	    // of this one, if needed.
	    // In this example, the CAN bus is set to 500 kHz.  In the function below,
	    // the call to SysCtlClockGet() is used to determine the clock rate that
	    // is used for clocking the CAN peripheral.  This can be replaced with a
	    // fixed value if you know the value of the system clock, saving the extra
	    // function call.  For some parts, the CAN peripheral is clocked by a fixed
	    // 8 MHz regardless of the system clock in which case the call to
	    // SysCtlClockGet() should be replaced with 8000000.  Consult the data
	    // sheet for more information about CAN peripheral clocking.
	    //
	    CANBitRateSet(CAN0_BASE, SysCtlClockGet(), 500000);

	    //
	    // Enable the CAN for operation.
	    //
	    CANEnable(CAN0_BASE);
}


void
system_init(void) {
		//
		// Enable lazy stacking for interrupt handlers.  This allows floating-point
		// instructions to be used within interrupt handlers, but at the expense of
		// extra stack usage.
		//
		ROM_FPULazyStackingEnable();

		//
		// Set the clocking to run directly from the crystal.
		//
		ROM_SysCtlClockSet(SYSCTL_SYSDIV_5|SYSCTL_USE_PLL|SYSCTL_OSC_MAIN|SYSCTL_XTAL_16MHZ);
		//
	    // Enable the GPIO port that is used for the on-board LED.
	    //
	    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

	    //
	    // Enable the GPIO pins for the LED (PF2).
	    //
	    ROM_GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_2);

	    //
		// Initialize the CAN 0.
		//
	    ConfigureCAN();

}


unsigned int offset = 0;
unsigned int clock = 0;


int
main(void)
{

//	unsigned char inChar;
	system_init();
	nmea_receiver_init(&receiver);
	//
	// Enable the peripherals used by this example.
	//
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART4);
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);

	//
	// Enable processor interrupts.
	//
	ROM_IntMasterEnable();

	//
	// Set GPIO A0 and A1 as UART pins.
	//
	GPIOPinConfigure(GPIO_PC4_U4RX);
	GPIOPinConfigure(GPIO_PC5_U4TX);
	ROM_GPIOPinTypeUART(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5);

	//
	// Configure the UART for 115,200, 8-N-1 operation.  ROM_SysCtlClockGet()
	//
	ROM_UARTConfigSetExpClk(UART4_BASE, SysCtlClockGet(), UART_SPEED,
							(UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
							 UART_CONFIG_PAR_NONE));

	//
	// Enable the UART interrupt.
	//
	ROM_IntEnable(INT_UART4);
	ROM_UARTIntEnable(UART4_BASE, UART_INT_RX | UART_INT_RT);

	//
	// Prompt for text to be entered.SysCtlClockGet()
	//
	UARTFIFODisable(UART4_BASE);

	SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
	GPIOPinConfigure(GPIO_PA0_U0RX);
	GPIOPinConfigure(GPIO_PA1_U0TX);
	GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
	UARTConfigSetExpClk(UART0_BASE, SysCtlClockGet(), UART_SPEED, (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));

	clock = ROM_SysCtlClockGet();

	//
	// Loop forever echoing data through the UART.
	//
    while (1)
    {
    	if (g_newCharOffset >= BUFF_SIZE)
    	{
    		g_newCharOffset = 0;
    	}

    	if (g_readedCharOffset >= BUFF_SIZE)
    	{
    		g_readedCharOffset = 0;
    	}

    	if (g_newCharOffset != g_readedCharOffset) {
    		UARTCharPutNonBlocking(UART0_BASE,  buff[g_readedCharOffset]);
    		nmea_receiver_put(&receiver, buff[g_readedCharOffset++], gps_receiver_callback);
    	}
    }

    nmea_receiver_destroy(&receiver);
}
